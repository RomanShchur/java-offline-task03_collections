package appLogic;

import CustomDeque.com.epam.trainings.CustomDeque;
import binaryTree.com.epam.trainings.BnaryTree;
import priorityQueue.com.epam.trainings.PriorityQueue;
import org.apache.logging.log4j.*;

public class Main {
  private static Logger logga = LogManager.getLogger();
  public static void main(String[] args) {
    BnaryTree btree = new BnaryTree();
    CustomDeque dque = new CustomDeque();
    PriorityQueue pque = new PriorityQueue(5);
    btree.insert(12);
    btree.insert(2);
    btree.insert(3);
    btree.insert(5);
    btree.insert(21);
    btree.display(btree.root);
    logga.info("\n");
    logga.info("\n" + btree.search(3));
    logga.info( "\n" +btree.delete(3));
    btree.display(btree.root);
    logga.info( "\n" +btree.delete(5));
    btree.display(btree.root);
    logga.info("\n" + btree.delete(21));
    btree.display(btree.root);
    logga.info("\n");

    dque.insertFirst(34);
    dque.insertLast(45);
    dque.show();
    dque.removeFirst();
    dque.removeFirst();
    dque.removeFirst();
    dque.show();
    dque.insertFirst(21);
    dque.insertFirst(98);
    dque.insertLast(5);
    dque.insertFirst(43);
    dque.show();
    dque.removeLast();
    dque.show();
    logga.info("\n");

    pque.add(34);
    pque.add(23);
    pque.add(5);
    pque.add(87);
    pque.add(32);
    pque.show();
    logga.info("\n");
    pque.get();
    pque.pull();
    pque.get();
    pque.pull();
    pque.get();
    pque.pull();
    pque.pull();
    pque.show();
    logga.info("\n");
  }
}
