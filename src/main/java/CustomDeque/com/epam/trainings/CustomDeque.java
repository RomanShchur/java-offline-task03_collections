package CustomDeque.com.epam.trainings;

import java.util.*;
import org.apache.logging.log4j.*;

public class CustomDeque<T> implements Queue<T> {
  private static Logger logga = LogManager.getLogger();
  private List<T> deque = new ArrayList<T>();
  public void insertFirst(T thing) {
    deque.add(0, thing);
  }
  public void insertLast(T thing) {
    deque.add(thing);
  }
  public boolean removeFirst() {
    if(deque.isEmpty()) {
      return false;
    }
    T rem = deque.remove(0);
    return true;
  }
  public boolean removeLast() {
    if(deque.isEmpty()){
      return false;
    }
    T rem = deque.remove(deque.size()-1);
    return true;
  }
  public T peekFirst() {
    T thing = deque.get(0);
    return thing;
  }
  public T peekLast() {
    T thing = deque.get(deque.size()-1);
    return thing;
  }

  public void show() {
    for (T item : deque) {
      logga.info(item + " ");
    }
  }

  public int size() {
    return 0;
  }

  public boolean isEmpty() {
    return false;
  }

  public boolean contains(Object o) {
    return false;
  }

  public Iterator<T> iterator() {
    return null;
  }

  public Object[] toArray() {
    return new Object[0];
  }

  public <T1> T1[] toArray(T1[] a) {
    return null;
  }

  public boolean add(T t) {
    return false;
  }

  public boolean remove(Object o) {
    return false;
  }

  public boolean containsAll(Collection<?> c) {
    return false;
  }

  public boolean addAll(Collection<? extends T> c) {
    return false;
  }

  public boolean removeAll(Collection<?> c) {
    return false;
  }

  public boolean retainAll(Collection<?> c) {
    return false;
  }

  public void clear() {

  }

  public boolean offer(T t) {
    return false;
  }

  public T remove() {
    return null;
  }

  public T poll() {
    return null;
  }

  public T element() {
    return null;
  }

  public T peek() {
    return null;
  }
}
