package priorityQueue.com.epam.trainings;

import java.util.*;
import org.apache.logging.log4j.*;

public class PriorityQueue<T> implements  Queue<T>{
  private static Logger logga = LogManager.getLogger();
  private Comparable[] pQueue;
  private int index;

  public PriorityQueue(int cap) {
    pQueue = new Comparable[cap];
  }
  public void add(Comparable thing ) {
    if (index == pQueue.length) {
      return;
    }
    pQueue[index] = thing;
    index++;
  }
  public Comparable pull(){
    if(index == 0){
      return null;
    }
    int maxIndex = 0;
    for (int i=1; i<index; i++) {
      if (pQueue[i].compareTo (pQueue[maxIndex]) > 0) {
        maxIndex = i;
      }
    }
    Comparable result = pQueue[maxIndex];
    index--;
    pQueue[maxIndex] = pQueue[index];
    return result;
  }
  public Comparable get() {
    if (index == 0) {
      return null;
    }
    int maxIndex = 0;
    for (int i=1; i<index; i++) {
      if (pQueue[i].compareTo (pQueue[maxIndex]) > 0) {
        maxIndex = i;
      }
    }
    Comparable result = pQueue[maxIndex];
    return result;
  }
  public void show(){
    for (Comparable value : pQueue) {
      logga.info(value + " ");
    }
  }

  public int size() {
    return 0;
  }

  public boolean isEmpty() {
    return false;
  }

  public boolean contains(Object o) {
    return false;
  }

  public Iterator<T> iterator() {
    return null;
  }

  public Object[] toArray() {
    return new Object[0];
  }

  public <T> T[] toArray(T[] a) {
    return null;
  }


  public boolean remove(Object o) {
    return false;
  }

  public boolean containsAll(Collection<?> c) {
    return false;
  }

  public boolean addAll(Collection<? extends T> c) {
    return false;
  }

  public boolean removeAll(Collection<?> c) {
    return false;
  }

  public boolean retainAll(Collection<?> c) {
    return false;
  }

  public void clear() {

  }

  public boolean offer(T e) {
    return false;
  }

  public boolean add(T t) {
    return false;
  }

  public T remove() {
    return null;
  }

  public T poll() {
    return null;
  }

  public T element() {
    return null;
  }

  public T peek() {
    return null;
  }
}
