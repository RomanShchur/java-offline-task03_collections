package binaryTree.com.epam.trainings;

import java.util.Collection;
import java.util.Iterator;
import org.apache.logging.log4j.*;

class Node {
  int data;
  Node left;
  Node right;
  public Node(int data) {
    this.data = data;
    left = null;
    right = null;
  }
}
public class BnaryTree implements Collection {
  private static Logger logga = LogManager.getLogger();
  public static  Node root;
  public BnaryTree() {
    this.root = null;
  }
  public void insert(int id) {
    Node newNode = new Node(id);
    if(root == null) {
      root = newNode;
      return;
    }
    Node current = root;
    Node parent = null;
    while(true) {
      parent = current;
      if(id<current.data) {
        current = current.left;
        if(current == null) {
          parent.left = newNode;
          return;
        }
      } else {
        current = current.right;
        if(current == null) {
          parent.right = newNode;
          return;
        }
      }
    }
  }
  public boolean search(int id) {
    Node current = root;
    while(current != null) {
      if(current.data == id) {
        return true;
      } else if(current.data > id) {
        current = current.left;
      } else {
        current = current.right;
      }
    }
    return false;
  }
  public boolean delete(int id) {
    Node parent = root;
    Node current = root;
    boolean isLeftChild = false;
    while(current.data != id) {
      parent = current;
      if(current.data > id) {
        isLeftChild = true;
        current = current.left;
      } else {
        isLeftChild = false;
        current = current.right;
      }
      if(current == null) {
        return false;
      }
    }
    if(current.left == null && current.right == null) {
      if(current == root) {
        root = null;
      }
      if(isLeftChild == true) {
        parent.left = null;
      } else {
        parent.right = null;
      }
    } else if(current.right == null) {
      if(current == root) {
        root = current.left;
      } else if(isLeftChild) {
        parent.left = current.left;
      } else {
        parent.right = current.left;
      }
    }
    else if(current.left == null) {
      if(current == root){
        root = current.right;
      } else if(isLeftChild) {
        parent.left = current.right;
      } else {
        parent.right = current.right;
      }
    } else if(current.left != null && current.right != null) {
      Node successor = getSuccessor(current);
      if(current == root) {
        root = successor;
      } else if(isLeftChild) {
        parent.left = successor;
      } else {
        parent.right = successor;
      }
      successor.left = current.left;
    }
    return true;
  }
  public void display(Node root) {
    if(root != null) {
      display(root.left);
      logga.info(" " + root.data);
      display(root.right);
    }
  }
  private Node getSuccessor(Node deleteNode) {
    Node successsor = null;
    Node successsorParent = null;
    Node current = deleteNode.right;
    while(current != null) {
      successsorParent = successsor;
      successsor = current;
      current = current.left;
    }
    if(successsor != deleteNode.right) {
      successsorParent.left = successsor.right;
      successsor.right = deleteNode.right;
    }
    return successsor;
  }
  private void hasNoChilds(int id) {

  }
  private void hasOneChild(int id) {

  }
  private void hasTwoChilds(int id) {

  }

  public int size() {
    return 0;
  }

  public boolean isEmpty() {
    return false;
  }

  public boolean contains(Object o) {
    return false;
  }

  public Iterator iterator() {
    return null;
  }

  public Object[] toArray() {
    return new Object[0];
  }

  public boolean add(Object o) {
    return false;
  }

  public boolean remove(Object o) {
    return false;
  }

  public boolean addAll(Collection c) {
    return false;
  }

  public void clear() {

  }

  public boolean retainAll(Collection c) {
    return false;
  }

  public boolean removeAll(Collection c) {
    return false;
  }

  public boolean containsAll(Collection c) {
    return false;
  }

  public Object[] toArray(Object[] a) {
    return new Object[0];
  }
}
